---
title: "Fahrplan"
slug: "fahrplan"
date: 2020-06-29
weight: 3
comments: false
pre: "<i class='fab fa-github'></i> "
---

Der ZMF Learning Hub ist ein flexibel System aus unterschiedlichen Diensten, die sich stetig verändern und sich an die benötigten Anforderungen anpassen lässt. Seht was bei uns gerade läuft und was wir bereits geschafft haben.

## 2021

- {{% fontawesome cogs %}} Landing Page für den Learning Hub und deren Services erstellen [www.hub.zmf.bayern](https://hub.zmf.bayern/)
- {{% fontawesome cogs %}} Corporate Identity und Corporate Design rund um den Learning Hub
- {{% fontawesome cogs %}} Einbindung von bis zu 5 Nodes in das Kubernetes Cloud Cluster mit anschließenden Notfall- und Stresstests
- {{% fontawesome cogs %}} [Kommunikationszentrale und Kurznachrichtendienst mit Rocket.Chat](https://www.chat.zmf.bayern)
- {{% fontawesome cogs %}} [Dokumentation zur Nutzung des ZMFLH für Kursersteller\*innen](https://zmf-bayern.gitlab.io/doku/)
- {{% fontawesome cogs %}} Erprobung des Videokonferenzsystems auf dem eigenen Cloud Cluster mit Skalierbarkeit auf bis zu 1000 gleichzeitigen Nutzenden
- {{% fontawesome stop-circle %}} Angebot von bis zu 100 kostenfreien und öffentlich zugänglichen Materialien (sog. [OER](https://open-educational-resources.de/was-ist-oer-3-2/)) für die pädagogische Arbeit mit Kindern
- {{% fontawesome stop-circle %}} Open Educational Repositorium für das StMAS und Kooperationspartnern mit der Software [Edu-Sharing](https://edu-sharing.com/)
- {{% fontawesome stop-circle %}} Integration und Durchführung von bis zu 80 Online Kursangebote für die Digitalisierungsstrategie des StMAS und Kooperationspartnern
- {{% fontawesome stop-circle %}} Aufbau einer Community rund um das Thema frühkindliche Bildung mit Fokus auf die Fachkräfte in Kitas
- {{% fontawesome stop-circle %}} Dokumentation der Services auf Gitlab.com
- {{% fontawesome check-square %}} Monitoring der Services mit Prometheus und Grafana
- {{% fontawesome check-square %}} [Videokonferenzsystem BigBlueButton mit Integration zu all unseren Diensten](https://www.video.zmf.bayern)
- {{% fontawesome check-square %}} [Kollaborative Dokumente mit einem Fokus auf *Distraction-Free Writing* mit HedgeDoc](https://www.notizen.zmf.bayern)

## 2020

- {{% fontawesome check-square %}} [Videokonferenzsystem BigBlueButton für maximal 200 Teilnehmende](https://www.video.zmf.bayern)
- {{% fontawesome check-square %}} [Kursangebote mit Moodle und einem UX-zentriertem Theme namens Snap](https://www.kurse.zmf.bayern)
- {{% fontawesome check-square %}} Umsetzung der Basis Dienste auf einem selbst gehosteten und eigens orchestriertem Kubernetes Cloud Cluster
- {{% fontawesome check-square %}} Entwicklungsumgebung aufsetzen für ein Kubernetes Cloud Cluster
- {{% fontawesome check-square %}} Datenschutzabkommen und AV-Vertrag mit einem Internetdienstleistungsunternehmen mit dem Server Standort Deutschland
- {{% fontawesome check-square %}} Wissenschaftliche Vorüberlegungen für die Konzeption und Erstellung eines zeitgemäßen Lehr-Lehr-Systems anhand von Evaluationen und Erfahrungen aus dem Pilotkurs: [Medienbildung in der pädagogischen Arbeit mit Kindern – Blended-Learning-Einführungskurs für Fachkräfte in Kitas](https://www.zmf.bayern/wp-content/uploads/ZMF-Abschlussbericht-Pilotkurs-2020.pdf)
