---
title: "Allgemein"
slug: "allgemein"
chapter: true
date: 2020-06-29
weight: 1
comments: false
pre: "<i class='fab fa-github'></i> <b>1. </b>"
---

### ZMF Learning Hub - hier wird gelernt

# Benutzungshandbuch

Willkommen beim Zentrum für Medienkompetenz in der Frühpädagogik. Hier lernst du alle Möglichkeiten des ZMF Learning Hubs kennen. Diese Dokumentation ist besonders für Kursleitende, Ed-Tech Interessierte und Mitarbeitende des StMAS und Kooperationspartnern des ZMF interessant.
