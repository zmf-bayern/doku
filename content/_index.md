---
title: "Dokumentation"
slug: "doku"
chapter: true
date: 2020-06-29
weight: 1
pre: "<i class='fab fa-github'></i> "
---

### Dokumentation und Leitfaden des ZMF Learning Hubs aus Kursanbieter\*innen Sicht.

# Lasst uns anfangen

Diese Dokumentation beschreibt aus Lehrenden- sowie Kurserstellendensicht, wie ein Kurs auf dem Learning Hub erstellt und angeboten werden kann. Jedes Kapitel wird als handlungsorientierter Leitfaden geschrieben und mit _good practise_ Beispielen dargestellt.

<!-- Für einen Schnelleinstieg in ein Kapitel, folgt nun die Gliederung der gesamten Dokumentation:

- [Allgemein](#)
  - [Dokumentation](#)
  - [Ansprechpersonen](#)
  - [Fahrplan](#)
  - [Legende](#)
- [Grundlagen](#)
  - [Schnelleinstieg](#)
  - [Kursbeantragung](#)
  - [Kursverwaltung](#)
  - [Kursaufbau](#)
  - [Teilnehmendenverwaltung](#)
- [Kursaufbau](#)
  - [Online Kurs](#)
  - [Blended Learning](#)
  - [Gestaltungsvorlage](#)
- [Inhalte](#)
  - [Grundbausteine](#)
  - [Medien einbinden](#)
  - [Interaktivität](#)
  - [Externe Dienste](#)
- [Kommunikation](#)
  - [Chatanwendung](#)
  - [Videokonferenz](#)
- [Kollaboration](#)
  - [Schreiben](#)
  - [Dateien austauschen](#) -->
