---
title: "Kommunikation"
slug: "kommunikation"
chapter: true
date: 2020-06-29
weight: 5
comments: false
draft: "true"
pre: "<i class='fab fa-github'></i> <b>5. </b>"
---

### Kapitel 2: Ein Kurs beim ZMF

# Kursleitung oder Kursanbietender

Dieses Kapitel behandelt zwei unterschiedliche Möglichkeiten für ein Kursangebot beim ZMF. Du möchtest entweder einen bestehenden Kurs leiten bzw. daran als Lehrende(r) mitwirken oder einen gänzlich neuen auf der ZMF Learning Hub anbieten.

<!-- ## Inhaltsverzeichnis

- [Einstieg](einstieg)
- [Formate](formate)
- [Aufbau](aufbau)
- [Grundlagen](grundlagen)
- [Administration](administration) -->
