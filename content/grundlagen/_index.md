---
title: "Grundlagen"
slug: "grundlagen"
chapter: true
date: 2020-06-29
weight: 2
comments: false
draft: "false"
pre: "<i class='fab fa-github'></i> <b>2. </b>"
---

### Schritt für Schritt zum eigenen Kurs

# Grundlagen

Dieses Kapitel beschäftigt sich vorwiegend mit einem Schnelleinstieg in den ZMF Learning Hub und wie Schritt für Schritt ein Kursangebot entsteht und durchgeführt werden kann.
