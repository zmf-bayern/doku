---
title: "Moodle"
slug: "moodle"
chapter: true
date: 2020-06-29
weight: 3
comments: false
pre: "<i class='fab fa-github'></i> "
---

### Kapitel 3: Infrastruktur und Services

# xxx

xxx

## Plugins

> Moodle Plugins Production durchgehen und nachtragn

### Einschreibung

- [Authentication: Enrolment key based self-registration](https://moodle.org/plugins/auth_enrolkey)

### Videokonferenz

- [BigBlueButton](https://moodle.org/plugins/mod_bigbluebuttonbn)
- [Webex](https://moodle.org/plugins/mod_webexactivity)

### Interaktivität

- [H5P](https://moodle.org/plugins/mod_hvp)
