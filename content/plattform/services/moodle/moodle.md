
Die Kursinhalte als auch ein Großteil der Kommunikation werden dabei über das Lernplattform [Moodle](https://de.wikipedia.org/wiki/Moodle) mit der [Gestaltungsvorlage (Theme) Snap](https://moodle.org/plugins/theme_snap) den Lernenden zur Verfügung gestellt.

- Hinweis Snap wird ca. 1 Jahr verspätet zum aktuellen Release aktualisiert.
- Basiert auf der Darstellung von Blackboard
