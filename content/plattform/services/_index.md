---
title: "Services"
slug: "services"
chapter: true
date: 2020-06-29
weight: 3
comments: false
pre: "<i class='fab fa-github'></i> "
---

### Was wir verwenden

# Services

## Übersicht

| Art | Service | URL | Status | Priorität |
|---|---|---|---|---|
| Analyse  | Matomo  | analyse.zmf.bayern  | in Planung | mittel |
| Chat  | Rocket.Chat | chat.zmf.bayern  | in Planung  | mittel  |
| LMS  | Moodle  | kurse.zmf.bayern  | in Betrieb | hoch |
| Monitoring  | Rancher  | rancher.zmf.bayern  | in Betrieb | hoch  |
| Videochat  | BigBlueButton  | video.zmf.bayern  | in Planung | hoch  |
| Videochat  | Jitsi  | konferenz.zmf.bayern  | in Planung | niedrig  |
