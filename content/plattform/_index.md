---
title: "Plattform"
slug: "plattform"
chapter: true
date: 2020-06-29
weight: 10
comments: false
draft: "true"
pre: "<i class='fab fa-github'></i> <b>12. </b>"
---

### Kapitel 3: Infrastruktur und Services

# Technische Dokumentation und Entwicklung

Dieses Kapitel ist die Software-, Hardware- und IT-Dokumentation des ZMF Learning Hubs. Hier wird die Infrastruktur beschrieben, welche Services wie laufen und ein kontinuierlicher Weiterentwicklungsprozess beschrieben.
