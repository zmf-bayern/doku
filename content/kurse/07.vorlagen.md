---
title: "Vorlagen"
slug: "vorlagen"
date: 2020-06-29
weight: 6
comments: false
pre: "<i class='fab fa-github'></i> "
---

## E-Mails

### Auto Response nach Anmeldung für einen Kurs

#### ZMF Kursanmeldung eingegangen

Liebe/r _**Anrede_ _Nachname**_,

herzlichen Dank für ihre Anmeldung zum Blended-Learning-Kurs "**_Kurstitel_**". Wir freuen uns über Ihr Interesse und geben Ihnen Kürze Bescheid, ob wir Ihre Anmeldung berücksichtigen konnten.

Herzliche Grüße,
das ZMF-Team

--
Manfred Steger

Wissenschaftlicher Referent
Zentrum für Medienkompetenz in der Frühpädagogik (ZMF) Amberg

____________


Tel.
09621 965 02 - 10
Fax
09621 965 02 - 99

E-Mail
kurse@zmf.bayern.de
Internet
www.zmf.bayern.de

Anschrift
Zentrum für Medienkompetenz in der Frühpädagogik
Köferinger Str. 1
D–92224 Amberg
