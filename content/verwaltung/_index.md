---
title: "Verwaltung"
slug: "verwaltung"
chapter: true
date: 2020-06-29
weight: 4
comments: false
draft: "false"
pre: "<i class='fab fa-github'></i> <b>4. </b>"
---

### Kurse und Teilnehmende verwalten und bewerten

# Verwaltung

Dieses Kapitel beschäftigt sich vorwiegend mit der Verwaltung von Kursen, Inhalten und Teilnehmende. Je nach Kursart und Ablauf müssen Inhalte verwaltet werden, damit z.B. Lerndaten gespeichert und von den Lehrenden eingesehen und bewertet werden kann.