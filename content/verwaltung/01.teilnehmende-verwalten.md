---
title: "Teilnehmende"
slug: "teilnehmende-verwalten"
date: 2020-06-29
weight: 1
comments: false
draft: "false"
pre: "<i class='fab fa-github'></i> "
---

## Rollen und Rechte in einem Kurs verwalten

Es gibt unterschiedliche Rollen in einem Kursangebot. Die wichtigsten sind:

- Kursersteller\*in
- Manager\*in
- Trainer\*in
- Trainer\*in ohne Bearbeitungsrechte
- Teilnehmende
- Gast

{{% notice tip %}}

Die Standardeinstellung ist für Kurserstellende die Rolle **Manager\*in**, für Lehrende **Trainer\*in** und für Teilnehmende die Rolle **Teilnehmende**.

{{% /notice %}}

Kursersteller\*in und Manager\*in können Rollen von Teilnehmenden verändern und Teilnehmende in ein bestehendes Kursangebot hinzufügen und entfernen.

## Rollen verwalten

Rollen können in einem Kurs über das Kursmenü angepasst werden. Gehe dazu in deinen Kurs und klicke auf das Zahnrad Symbol {{% fontawesome cog %}} oben rechts im Hauptmenü. Wähle im aufklappenden Seitenmenü den Eintrag _Nutzer\*innen > Eingeschrieben Nutzer\*innen_ aus. Es erscheint nun eine Liste mit eingeschrieben Personen.

In der Spalte **Rollen** werden die Berechtigungen der jeweiligen Person angezeigt. Eine Person kann mehrere Rollen besitzen. Damit die Rolle verändert werden kann, klicke auf das {{% fontawesome pen %}} `Stift Symbol` in der Spalte der Person. Klicke auf das erscheinende Dropdown Menü und wähle die gewünschte Rolle aus. Für das entfernen einer Rolle kannst du den das kleine `X Symbol` bei der jeweiligen Rolle des Nutzenden anklicken. Klicke am Schluss noch auf das {{% fontawesome save %}} `Disketten Symbol` zum Speichern der Rollenänderung.

![Schritt für Schritt Anleitung wie Rollen von Kursteilnehmenden verändert werden können. Anleitung wie oben beschrieben.](../verwaltung/rollen-veraendern.gif)

## Nutzende hinzufügen und entfernen

Damit Lernende in einen Kurs ein- bzw. ausgeschrieben werden können, gehe dazu in deinen Kurs und klicke auf das Zahnrad Symbol {{% fontawesome cog %}} oben rechts im Hauptmenü. Wähle im aufklappenden Seitenmenü den Eintrag _Nutzer\*innen > Eingeschrieben Nutzer\*innen_ aus. Klicke auf den `Button: Nutzer\*innen einschreiben`. Klicke im Popup Fenster unter dem Eintrag _Nutzer\*in auswählen_ in das Suchenfeld und gebe den Namen der Person ein. Klicke anschließend auf den `Button: Ausgewählte Nutzer\*innen und globale Gruppen einschreiben`. Die hinzugefügten Nutzenden erscheinen nun in der Liste der Teilnehmenden.

![Schritt für Schritt Anleitung wie Teilnehmende ein einen Kurs eingeschrieben werden können. Anleitung wie oben beschrieben.](../verwaltung/teilnehmene-einschreiben.gif)

Möchtest du einen Nutzenden ausschreiben, klicke auf das {{% fontawesome trash-alt %}} `Mülleimer Symbol` in der Liste rechts neben dem Namen des jeweiligen Teilnehmenden.

![Schritt für Schritt Anleitung wie Teilnehmende aus einem Kurs entfernt werden können. Anleitung wie oben beschrieben.](../verwaltung/teilnehmene-ausschreiben.gif)
